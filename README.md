## Prerequisites

* Python 2.7
* SQLite 3

Test deployment
---------------
	$ git clone https://haseebbit@bitbucket.org/haseebbit/flask-blog-example.git
	$ cd ~/flask-blog-example
	$ pip install -r requirements.txt

Create database in SQLite3 
-------------------------
	$ sqlite3 /tmp/flaskr.db < schema.sql
	
Fireup the python shell
-----------------------
	>>> from flaskr import init_db
	>>> init_db()
	
Run the server
--------------
	$ python flaskr.py	

